﻿using System;
using System.Data.SQLite;
using System.Collections;
using System.IO;

namespace BCWindowService
{
    class DataBaseUtil
    {
        private SQLiteConnection sqlite;
        private string tableName = "transactionTable";
        private string WSConfig = "WSConfigTable";

        public DataBaseUtil() { }

        /**
         * Writes a given batch of formatted data to the database
         */
        public void WriteBatchToDB(ArrayList batch)
        {
            SQLiteCommand sqlite_cmd;

            foreach (string[] row in batch)
            {
                string query = "INSERT INTO " + tableName + " VALUES (";

                //Adds the data literals
                for(int i = 0; i <  row.Length-1; i++)
                {
                    query = query + " '" + row[i]+"',";
                }

                int index = row.Length - 1;
                query = query + " '" + row[index] + "')";

                //Create command
                sqlite_cmd = sqlite.CreateCommand();
                sqlite_cmd.CommandText = query;

                //execute query
                sqlite_cmd.ExecuteNonQuery();
            }
        }

        /**
         * Retrieves the row data from the database given a file name
         * 
         * This function performs a query which assumes that file names are unique values. 
         * 
         * Returns an ArrayList conatiaing the File Name, Last Modified, and Size of the given file
         * or returns an empty ArrayList if an entry could not be found. This should be used to check
         * if an entry was found in the calling code.
         * 
         * Query runs on the WSConfig Table.
         */
        public ArrayList GetFileInfoFromDB(string fileName)
        {
            SQLiteCommand sqlite_cmd;
            SQLiteDataReader sqReader;
            ArrayList result = new ArrayList();

            string query = @"SELECT * FROM " + WSConfig + " WHERE `File Name`='"+fileName+"'";
            sqlite_cmd = sqlite.CreateCommand();
            sqlite_cmd.CommandText = query;
            sqReader = sqlite_cmd.ExecuteReader();

            while(sqReader.Read() )
            {
                result.Add(sqReader.GetString(0));
                result.Add(sqReader.GetString(1));
                result.Add(sqReader.GetInt64(2));
            }
  
            return result;
        }

        /**
         * Creates and exeuctes a query to update the given file's information
         * 
         * This function executes a query which assumes that the file name is a unique value. The 
         * query runs on the WSConfig Table
         */
        public void UpdateFileData(FileInfo fileInfo)
        {
            SQLiteCommand sqlite_cmd;
            string query = "UPDATE " + WSConfig 
                + " SET `File Name`='" + fileInfo.Name 
                + "', `Last Modified`='" + fileInfo.LastWriteTime.ToString() 
                + "', `Size`='"+fileInfo.Length+"'" +
                " WHERE `File Name`='" + fileInfo.Name + "' ";

            sqlite_cmd = sqlite.CreateCommand();
            sqlite_cmd.CommandText = query;
            sqlite_cmd.ExecuteNonQuery();
        }

        /**
         * Inserts the given File information into the WSConfig DB.
         * 
         * This function should be called after a call to GetFileInfoFromDB and only if the function
         * returned an empty ArrayList (indicating a failed lookup)
         */
        public void InsertFileData(FileInfo fileInfo)
        {
            SQLiteCommand sqlite_cmd;
            string query = "INSERT INTO '" +  WSConfig 
                + "' VALUES ('"+fileInfo.Name+ "', '" + fileInfo.LastWriteTime.ToString() + "', '" + fileInfo.Length + "')";

            sqlite_cmd = sqlite.CreateCommand();
            sqlite_cmd.CommandText = query;
            sqlite_cmd.ExecuteNonQuery();
        }

        /**
         * Handles closing the db connection
         */
        public void Stop()
        {
            sqlite.Close();
        }

        /**
         * Attempts to connect to the database. Returns true on success, false otherwis.
         * 
         * Opens a connection to the database. Then checks if the 'tableName' and 'WSConfig' tables exists, if either
         * does not exists the function creates them.
         */
        public bool Connect()
        {
            SQLiteCommand sqlite_cmd;

            string[] columns = { "Entry Date", "Client Name", "Client Code", "GL Account Number", "Transaction Amount" };
            string[] dataTypes = { "DATE", "VARCHAR(20)", "VARCHAR(20)", "INT", "FLOAT" };

            //Setup Connection
            string dbfile = "C:\\Users\\brtyu\\Documents\\databases\\binarycity.db";
            try
            {
                sqlite = new SQLiteConnection("Data Source=" + dbfile);
                sqlite.Open(); //Comment out this line to simulate unable to connect

                //Query to check if table exists create one if it doesnt
                string createTableQuery = "create table if not exists " + tableName + " (";

                for (int i = 0; i < columns.Length - 1; i++)
                {
                    createTableQuery = createTableQuery + " '" + columns[i] + "' " + dataTypes[i] + ",";
                }

                int index = columns.Length - 1;
                createTableQuery = createTableQuery + " '" + columns[index] + "' " + dataTypes[index] + ")";

                sqlite_cmd = sqlite.CreateCommand();
                sqlite_cmd.CommandText = createTableQuery;
                sqlite_cmd.ExecuteNonQuery();

                //Checks if WSConfig table exists, creates it if it doesnt
                createTableQuery = "create table if not exists " + WSConfig + " ('File Name' VARCHAR(50), 'Last Modified' VARCHAR(50), 'Size' BIGINT)";

                sqlite_cmd = sqlite.CreateCommand();
                sqlite_cmd.CommandText = createTableQuery;
                sqlite_cmd.ExecuteNonQuery();

                return true;
            } catch(Exception e)
            {
                return false;
            } 
            
        }
    }
}
