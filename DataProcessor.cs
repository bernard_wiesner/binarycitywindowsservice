﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BCWindowService
{
    class DataProcessor
    {
        Queue<FileInfo> filesToProcess = new Queue<FileInfo>();
        private string processDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "OUTPUT");
        DataBaseUtil dataBaseUtil;
        private int batchSize = 100;

        public DataProcessor(DataBaseUtil dataBaseUtil)
        {
            this.dataBaseUtil = dataBaseUtil;
        }

        /**
         * Gets the FileInfo of all the files in the processing folder
         * 
         * Must be called before StartProcessing function to ensure file
         * information is loaded before processing starts.
         */
        public void LoadFiles()
        {
            string[] fileNames = Directory.GetFiles(processDir);

            foreach (string fileName in fileNames)
            {
                filesToProcess.Enqueue(new FileInfo(Path.Combine(processDir, fileName)));
            }
        }

        /**
         * Starts the processing of the loaded files
         * 
         * Must be called after the LoadFiles in instance of this created to ensure
         * that the files data is loaded before processing starts.
         */
        public void StartProcessing()
        {
            //Process each file in queue
            while(filesToProcess.Count != 0)
            {
                OpenAndReadFile(filesToProcess.Dequeue());
            }
        }

        /**
         * Opens and read all the lines from the given file
         * 
         * Reads all of the data and puts the lines into a List of type string and processes
         * the lines in batches of 100 set with the batch size variable. Writes lines not written
         * back to the file if the connection to the DB is interrupted or the process is stopped.
         */
        protected void OpenAndReadFile(FileInfo currentFile)
        {
            string filePath = Path.Combine(processDir, currentFile.Name);
            List<string> lines = File.ReadAllLines(filePath).ToList<string>();

            //Remove the first line
            lines.RemoveAt(0);

            ArrayList formattedBatch = new ArrayList();
            ArrayList unformattedList = new ArrayList(); // Used if their is in interupt

            int count = 0;
            try
            {
                while (lines.Count != 0 )
                {
                    string nextRow = lines[count];

                    formattedBatch.Add(FormatRow(nextRow));
                    unformattedList.Add(nextRow);

                    if (formattedBatch.Count == batchSize)
                    {
                        
                        //Write Batch to DB
                        dataBaseUtil.WriteBatchToDB(formattedBatch);

                        //Clear ArrayList
                        formattedBatch.Clear();
                        unformattedList.Clear();
                    }

                    lines.RemoveAt(0);
                }

                if (formattedBatch.Count != 0)
                {
                    //Write Last Few Entries to DB
                    dataBaseUtil.WriteBatchToDB(formattedBatch);
                }

                currentFile.Delete(); //remove file on transfer complete
            } catch (Exception)
            {
                currentFile.Delete();

                foreach (string line in  unformattedList)
                {
                    //adds lines that were processed but not written back to lines
                    //unordered
                    lines.Add(line);
                }
                
                File.WriteAllLines(filePath, lines);
            }

        }

        /**
         * Deprecated function which does not handle writing back to file on connection broken
         */
        protected void OpenAndReadFileStream(FileInfo currentFile)
        {
            string filePath = Path.Combine(processDir, currentFile.Name);

            StreamReader reader = new StreamReader(filePath);

            string firstLine = reader.ReadLine();

            ArrayList formattedBatch = new ArrayList();

            string nextRow;
            
            while ((nextRow = reader.ReadLine()) != null)
            {
                formattedBatch.Add(FormatRow(nextRow));

                if (formattedBatch.Count == batchSize) {
                    //Write Batch to DB
                    dataBaseUtil.WriteBatchToDB(formattedBatch);
                    //Clear ArrayList
                    formattedBatch.Clear();
                }
            }

            if (formattedBatch.Count != 0)
            {
                dataBaseUtil.WriteBatchToDB(formattedBatch);
                //Write Last Few Entries to DB
            }

            reader.Close();

            currentFile.Delete();
           
        }

        /**
         * Formats the row according to the specifications
         * 
         * Calls the function needed to do the formatting
         */
        protected string[] FormatRow(string row)
        {
            string[] elements = row.Split(',');
            
            string date = elements[0];
            date = FormatDate(date);
            string accountNum = elements[3];
            accountNum = FormatAccountNum(accountNum);
            elements[0] = date;
            elements[3] = accountNum;
            return elements;
        }

        /**
         * Formats the date
         * 
         * Changes date from DDMMYYYY to YYYY-MM-DD
         */
        protected string FormatDate(string date)
        {
            string day = date.Substring(0, 2);
            string month = date.Substring(2, 2);
            string year = date.Substring(4);

            return year + "-" + month + "-" + day;
        }

        /**
         * Removes leading zeroe from accountNum
         */
        protected string FormatAccountNum(string accountNum)
        {
            return accountNum.TrimStart(new Char[] { '0' });

        }
    }
}
