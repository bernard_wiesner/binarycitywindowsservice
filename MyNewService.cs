﻿using System;
using System.Collections;
using System.Diagnostics;
using System.ServiceProcess;
using System.Timers;
using System.IO;
using System.Runtime.InteropServices;
using System.Net.Mail;
using System.Threading;
using System.Net;

namespace BCWindowService
{
    //Services report their status to the Service Control Manager so 
    //that a user can tell whether a service is functioning correctly
    public enum ServiceState
    {
        SERVICE_STOPPED = 0x00000001,
        SERVICE_START_PENDING = 0x00000002,
        SERVICE_STOP_PENDING = 0x00000003,
        SERVICE_RUNNING = 0x00000004,
        SERVICE_CONTINUE_PENDING = 0x00000005,
        SERVICE_PAUSE_PENDING = 0x00000006,
        SERVICE_PAUSED = 0x00000007,
    }

    //The Service Control Manager uses the dwWaitHint and dwCheckpoint
    //members of the SERVICE_STATUS structure to determine how much time
    //to wait for a Windows service to start or shut down.If your 
    //OnStart and OnStop methods run long, your service can request more 
    //time by calling SetServiceStatus again with an incremented 
    //dwCheckPoint value.
    [StructLayout(LayoutKind.Sequential)]
    public struct ServiceStatus
    {
        public int dwServiceType;
        public ServiceState dwCurrentState;
        public int dwControlsAccepted;
        public int dwWin32ExitCode;
        public int dwServiceSpecificExitCode;
        public int dwCheckPoint;
        public int dwWaitHint;
    };

    public partial class MyNewService : ServiceBase
    {
        //contains the identifier of the next event to write into the event log
        private int eventId = 1;
        private FileSystemWatcher watcher;
        private string directoryToWatch = "C:\\Users\\brtyu\\Documents\\INPUT";
        private DataBaseUtil dataBaseUtil;
        private System.Timers.Timer timer;
        private string processDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "OUTPUT");
        private int tries = 0;
        private bool isEmpty = true;

        //defines SetServiceStatus function through a platform invoke
        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(System.IntPtr handle, ref ServiceStatus serviceStatus);

        public MyNewService()
        {
            InitializeComponent();
            this.watcher = new FileSystemWatcher();

            //Create DB Util
            dataBaseUtil = new DataBaseUtil();
            eventLog1 = new System.Diagnostics.EventLog();

            if (!System.Diagnostics.EventLog.SourceExists("MySource"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "MySource", "MyNewLog");
            }

            eventLog1.Source = "MySource";
            eventLog1.Log = "MyNewLog";
        }

        protected override void OnStart(string[] args)
        {
            eventLog1.WriteEntry("In OnStart!!");

            // Update the service state to Start Pending.
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            if (Directory.GetFiles(processDir).Length != 0)
            {
                //Check if processing folder empty on start
                isEmpty = false;
            };

            //Try to connect to DB
            if (!dataBaseUtil.Connect())
            {
                //Try two connection attempts
                bool connected = false;

                eventLog1.WriteEntry("Could not connect to DB", EventLogEntryType.Error, eventId++);
                
                for (int i = 0; i < 2; i++)
                {
                    eventLog1.WriteEntry("Retrying... "+(i+1), EventLogEntryType.Error, eventId++);

                    if (dataBaseUtil.Connect())
                    {
                        eventLog1.WriteEntry("Connection Successful!" , EventLogEntryType.Information, eventId++);

                        connected = true;
                        //Connected reset try counter
                        tries = 0;
                        break;
                    };
                }

                if (!connected)
                {
                    eventLog1.WriteEntry("Could not connect to DB. Sending email and terminating", EventLogEntryType.Error, eventId++);

                    SendEmail();
                    //Kills service ded
                    this.Stop();
                }
            }


            // Set up a timer that triggers every minute.
            timer = new System.Timers.Timer();
            timer.Interval = 60000; // 300 seconds - 5min
            timer.Elapsed += new ElapsedEventHandler(this.OnTimer);
            timer.Start();

            // Update the service state to Running.
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        protected override void OnStop()
        {
            //Adds entry to event log when service stops
            eventLog1.WriteEntry("In OnStop.");
            dataBaseUtil.Stop();
            timer.Stop();

            // Update the service state to Stop Pending.
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_STOP_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            // Update the service state to Stopped.
            serviceStatus.dwCurrentState = ServiceState.SERVICE_STOPPED;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

        }

        /**
         * Handles the timer elapsed event and starts the source file check and manages
         * when to start processing files.
         * 
         * The method first checks if the inStart method found a file in the processing folder.
         * If there is a file in the process folder then a dataProcessor is started to read the
         * data in the folder.
         * 
         * If the processing folder was empty onStart then a second check (which works during runt time)
         * checks if the processing folder is empty. If the processing folder is not empty it is safe to
         * assume that there is a dataProcessor working in the directory and thus skip a cycle so that
         * the processor can finish its work.
         * 
         * To see if a dataProcessor needs to be started the source dir is checked for files. Next each file
         * is checked against the WSConfigDB and the data fetched if present. If the 'last modified' or 'size'
         * differs between the database entry and the file, or the file does not exist in the DB then the a
         * process request if fired. Otherwise there was no changes to the source dir and the program continues
         * to the next cycle.
         */
        public void OnTimer(object sender, ElapsedEventArgs args)
        {
            //Check if processing dir is empty - skip if not empty

            //Check if their are any files in process folder that still need to be processed
            //isEmpty can only be set to false in onStart so it is only an initial check and 
            //only runs once per service life cycle
            if (!isEmpty)
            {
                //Skip one cycle to finish transfer
                //Through a new data processor
                StartProcessor();
                isEmpty = true;
                return;
            };

            if (Directory.GetFiles(processDir).Length != 0)
            {
                //Skip one cycle to finish current transfer
                return;
            };

            //Get info of files in INPUT dir
            string[] files = Directory.GetFiles(directoryToWatch);
            
            foreach (string fileName in files) {

                FileInfo f = new FileInfo(Path.Combine(directoryToWatch, fileName));

                ArrayList dbInfo = dataBaseUtil.GetFileInfoFromDB(f.Name);

                if (dbInfo.Count == 0)
                {
                    eventLog1.WriteEntry("New File! ("+f.Name+") Preparing for processing...", EventLogEntryType.Information, eventId++);

                    //Write file info to DB
                    dataBaseUtil.InsertFileData(f);

                    //Transfer new data
                    HandleProcessRequest(f);

                }
                else
                {
                    //Check if the last modified or size has changed
                    //dbInfo[0] = file name
                    //dbInfo[1] = last modified
                    //dbInfo[2] = size (long)
                    if (!dbInfo[1].Equals(f.LastWriteTime.ToString()) || (long)dbInfo[2] != f.Length)
                    {
                        eventLog1.WriteEntry("File (" + f.Name + ") has changed! Preparing for processing...", EventLogEntryType.Information, eventId++);
                        HandleProcessRequest(f);
                    }
                }
             }
            
        }

        /**
         * Handles a request to format and send data to Database
         * 
         * Calls the CopyFilesToProcessingFolder function which copies the given file to the processing directory
         * then calls the StartProcess function to start formatting and writing of data to DB. Finally finally
         * updates the file entry in WSConfigDB with the new file information
         */
        protected void HandleProcessRequest(FileInfo f)
        {
            eventLog1.WriteEntry("Starting Processor", EventLogEntryType.Information, eventId++);

            //Start copy and DB update
            CopyFilesToProcessingFolder(f);
            
            StartProcessor();

            //update WSConfig data entry
            dataBaseUtil.UpdateFileData(f);

        }

        /**
         * Starts a seperate thread to handle DataProcessing so that the main thread does not interrupt the process
         */
        protected void StartProcessor()
        {
            //Start Processing on file
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                DataProcessor dp = new DataProcessor(dataBaseUtil);
                dp.LoadFiles();
                dp.StartProcessing();
            }).Start();
            
        }

        /**
         * Copies the given file to the given processing directory
         * 
         * The processing folder is set as OUTPUT, if it is not found one is created.
         */
        private void CopyFilesToProcessingFolder(FileInfo fi)
        {
            string workingDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string output_dir = Path.Combine(workingDirectory, "OUTPUT");

            if (!Directory.Exists(output_dir))
            {
                Directory.CreateDirectory(output_dir);
            }

                if (fi.Exists)
                    //TODO this will very likely cause problems if the destination has not completed resolving the file
                    //true allows file to be overwritten
                    fi.CopyTo(Path.Combine(output_dir, fi.Name), true);              
        }

        /**
         * Sends an email stating that the Windows Service could not connect to the DB
         * 
         * It doesnt really work because of Google's Security restrictions :(
         * Got this straight from StackOverflow
         */
        protected void SendEmail()
        {
            var fromAddress = new MailAddress("brtyu941@gmail.com", "Bernard");
            var toAddress = new MailAddress("brtyu941@gmail.com", "Bernard");
            const string fromPassword = "";
            const string subject = "DBBroke";
            const string body = "DB Broke plz help";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                UseDefaultCredentials = false,
            };

            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                smtp.Send(message);
            }
        }
    }
}
